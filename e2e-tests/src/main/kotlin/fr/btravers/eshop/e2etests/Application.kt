package fr.btravers.eshop.e2etests

import org.junit.runner.JUnitCore
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import kotlin.reflect.jvm.jvmName

@SpringBootApplication
@ConfigurationPropertiesScan
class Application

fun main() {
    JUnitCore.main(CucumberTest::class.jvmName)
}
