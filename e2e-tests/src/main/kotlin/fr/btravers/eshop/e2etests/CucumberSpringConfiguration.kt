package fr.btravers.eshop.e2etests

import io.cucumber.spring.CucumberContextConfiguration
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [Application::class])
@CucumberContextConfiguration
class CucumberSpringConfiguration
