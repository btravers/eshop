package fr.btravers.eshop.e2etests

import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
        glue = ["fr.btravers.eshop.e2etests"],
        features = ["classpath:features"],
        plugin = ["pretty", "html:target/cucumber-reports.html"]
)
class CucumberTest
