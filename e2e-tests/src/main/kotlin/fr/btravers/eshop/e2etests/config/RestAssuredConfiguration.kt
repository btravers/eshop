package fr.btravers.eshop.e2etests.config

import com.fasterxml.jackson.databind.ObjectMapper
import io.restassured.RestAssured
import io.restassured.config.ObjectMapperConfig
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.annotation.Configuration
import java.lang.reflect.Type

@Configuration
class RestAssuredConfiguration(private val mapper: ObjectMapper) : InitializingBean {

    override fun afterPropertiesSet() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
        RestAssured.useRelaxedHTTPSValidation()
        RestAssured.config().objectMapperConfig(ObjectMapperConfig().jackson2ObjectMapperFactory { _: Type, _: String -> mapper })
    }

}