package fr.btravers.eshop.e2etests

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(value = "application", ignoreUnknownFields = false)
data class ApplicationProperties(
        val url: String
)
