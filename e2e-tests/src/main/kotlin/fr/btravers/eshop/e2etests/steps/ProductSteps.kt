package fr.btravers.eshop.e2etests.steps

import fr.btravers.eshop.e2etests.ApplicationProperties
import fr.btravers.eshop.e2etests.service.Context
import fr.btravers.eshop.e2etests.service.ScenarioContextService
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import java.math.BigInteger

class ProductSteps(private val scenarioContextService: ScenarioContextService,
                   private val applicationProperties: ApplicationProperties) {

    @Given("^a user is authorized to add a new product$")
    fun `a user is authorized to add a new product`() {
    }

    @When("^the user add a new product$")
    fun `the user add a new product`() {
        val response = given()
                .baseUri(applicationProperties.url)
                .basePath("/products")
                .contentType(ContentType.JSON)
                .body(
                        mapOf(
                                "name" to "Test",
                                "price" to BigInteger("42")
                        )
                ) // fixme wip
                .post()

        scenarioContextService.context = Context(
                response = response
        )
    }

    @Then("^the request .* returns a status code (\\d{3})$")
    fun `the request () returns a status code {int}`(statusCode: Int) {
        scenarioContextService.context.response.then().statusCode(statusCode)
    }

}