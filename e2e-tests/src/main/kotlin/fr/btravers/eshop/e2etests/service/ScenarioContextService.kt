package fr.btravers.eshop.e2etests.service

import io.cucumber.spring.ScenarioScope
import io.restassured.response.Response
import org.springframework.stereotype.Service

@Service
@ScenarioScope
class ScenarioContextService {

    lateinit var context: Context

}

data class Context(
        val response: Response
)