# language: en
Feature: 01 - Add new product

  Scenario: Add new product
    Given a user is authorized to add a new product
    When the user add a new product
    Then the request is executed successfully and returns a status code 201