package fr.btravers.eshop.product

import kotlinx.coroutines.flow.Flow
import java.util.*

class ProductService(private val productRepository: ProductRepository) {

    suspend fun addProduct(product: Product): Product {
        return productRepository.save(product)
    }

    fun getAllProducts(): Flow<Product> {
        return productRepository.findAll()
    }

    suspend fun getProductById(id: UUID): Product {
        return productRepository.findById(id)
                ?: throw MissingProductException()
    }

    suspend fun deleteProductById(id: UUID) {
        val exists = productRepository.existsById(id)
        if (!exists) {
            throw MissingProductException()
        }
        productRepository.deleteById(id)
    }

    suspend fun updateProductById(id: UUID, product: Product): Product {
        val exists = productRepository.existsById(id)
        if (!exists) {
            throw MissingProductException()
        }
        return productRepository.save(product)
    }

}