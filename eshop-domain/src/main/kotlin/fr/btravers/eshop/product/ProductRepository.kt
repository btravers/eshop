package fr.btravers.eshop.product

import kotlinx.coroutines.flow.Flow
import java.util.*

interface ProductRepository {

    suspend fun save(product: Product): Product

    fun findAll(): Flow<Product>

    suspend fun findById(id: UUID): Product?

    suspend fun existsById(id: UUID): Boolean

    suspend fun deleteById(id: UUID)

}