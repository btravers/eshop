package fr.btravers.eshop.product

import java.math.BigInteger
import java.util.*

data class Product(
        val id: UUID? = null,
        val name: String,
        val price: BigInteger
)
