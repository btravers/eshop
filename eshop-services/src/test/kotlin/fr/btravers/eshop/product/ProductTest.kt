package fr.btravers.eshop.product

import fr.btravers.eshop.product.infrastructure.ProductEntity
import fr.btravers.eshop.product.infrastructure.SpringDataR2dbcProductRepository
import fr.btravers.eshop.utils.IntegrationTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import reactor.test.StepVerifier
import java.math.BigInteger
import java.util.*

@IntegrationTest
class ProductTest {

    @Autowired
    private lateinit var productRepository: SpringDataR2dbcProductRepository

    @Test
    fun `should add a new product`() {
        given()
                .basePath("/products")
                .contentType(ContentType.JSON)
                .body(
                        mapOf(
                                "name" to "Test",
                                "price" to BigInteger("42")
                        )
                )
        .`when`()
                .post()
        .then()
                .statusCode(201)
                .header("Location", notNullValue())
    }

    @Nested
    inner class WithExistingProduct {

        private lateinit var product: ProductEntity

        @BeforeEach
        fun setup() {
            val createProduct = productRepository.save(
                    ProductEntity(
                            name = "Test",
                            price = BigInteger("42")
                    )
            )
                    .doOnNext { p -> product = p }

            StepVerifier.create(createProduct)
                    .expectNextCount(1)
                    .verifyComplete()
        }

        @Test
        fun `should get all products`() {
            given()
                    .basePath("/products")
            .`when`()
                    .get()
            .then()
                    .statusCode(200)
                    .body("size()", equalTo(1))
                    .body("id", hasItem(product.id.toString()))
                    .body("name", hasItem("Test"))
                    .body("price", hasItem(42))
        }

        @Test
        fun `should get a product by id`() {
            given()
                    .basePath("/products")
            .`when`()
                    .get(product.id.toString())
            .then()
                    .statusCode(200)
                    .body("id", equalTo(product.id.toString()))
                    .body("name", equalTo("Test"))
                    .body("price", equalTo(42))
        }

        @Test
        fun `should update a product by id`() {
            given()
                    .basePath("/products")
                    .contentType(ContentType.JSON)
                    .body(
                            mapOf(
                                    "id" to product.id,
                                    "name" to "Updated test",
                                    "price" to BigInteger("92")
                            )
                    )
            .`when`()
                    .put(product.id.toString())
            .then()
                    .statusCode(200)
                    .body("id", equalTo(product.id.toString()))
                    .body("name", equalTo("Updated test"))
                    .body("price", equalTo(92))
        }

        @Test
        fun `should delete a product by id`() {
            given()
                    .basePath("/products")
            .`when`()
                    .delete(product.id.toString())
            .then()
                    .statusCode(204)
        }

    }

    @Nested
    inner class Error {

        @Test
        fun `should return a 404 when getting a missing product`() {
            given()
                    .basePath("/products")
            .`when`()
                    .get(UUID.randomUUID().toString())
            .then()
                    .statusCode(404)
        }

        @Test
        fun `should return a 400 when getting a product with a mal formatted id`() {
            given()
                    .basePath("/products")
            .`when`()
                    .get("1")
            .then()
                    .statusCode(400)
        }

        @Test
        fun `should return a 404 when deleting a missing product`() {
            given()
                    .basePath("/products")
            .`when`()
                    .delete(UUID.randomUUID().toString())
            .then()
                    .statusCode(404)
        }

        @Test
        fun `should return a 400 when deleting a product with a mal formatted id`() {
            given()
                    .basePath("/products")
            .`when`()
                    .delete("1")
            .then()
                    .statusCode(400)
        }

        @Test
        fun `should return a 404 when updating a missing product`() {
            given()
                    .basePath("/products")
                    .contentType(ContentType.JSON)
                    .body(
                            mapOf(
                                    "id" to UUID.randomUUID(),
                                    "name" to "Updated test",
                                    "price" to BigInteger("92")
                            )
                    )
            .`when`()
                    .put(UUID.randomUUID().toString())
            .then()
                    .statusCode(404)
        }

        @Test
        fun `should return a 400 when updating a product with a mal formatted id`() {
            given()
                    .basePath("/products")
                    .contentType(ContentType.JSON)
                    .body(
                            mapOf(
                                    "id" to UUID.randomUUID(),
                                    "name" to "Updated test",
                                    "price" to BigInteger("92")
                            )
                    )
            .`when`()
                    .put("1")
            .then()
                    .statusCode(400)
        }

    }

}