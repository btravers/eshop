package fr.btravers.eshop.utils

import io.restassured.RestAssured
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener

class RestAssuredManager : TestExecutionListener {

    override fun beforeTestMethod(testContext: TestContext) {
        RestAssured.port = testContext.applicationContext.environment.getRequiredProperty("local.server.port", Int::class.java)
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }

}