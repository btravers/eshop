package fr.btravers.eshop.utils

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestExecutionListeners(
        mergeMode = MERGE_WITH_DEFAULTS,
        listeners = [
            PostgresqlManager::class,
            RestAssuredManager::class
        ]
)
@Retention(AnnotationRetention.RUNTIME)
annotation class IntegrationTest
