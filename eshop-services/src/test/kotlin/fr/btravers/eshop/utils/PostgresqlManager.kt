package fr.btravers.eshop.utils

import org.flywaydb.core.Flyway
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.PostgreSQLContainerProvider

class PostgresqlManager : TestExecutionListener {

    companion object {
        private val POSTGRESQL_CONTAINER = PostgreSQLContainerProvider().newInstance("13.1")

        init {
            POSTGRESQL_CONTAINER.start()

            System.setProperty("spring.r2dbc.url", String.format("r2dbc:postgresql://%s:%s/%s", POSTGRESQL_CONTAINER.getHost(), POSTGRESQL_CONTAINER.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT), POSTGRESQL_CONTAINER.getDatabaseName()))
            System.setProperty("spring.r2dbc.username", POSTGRESQL_CONTAINER.getUsername())
            System.setProperty("spring.r2dbc.password", POSTGRESQL_CONTAINER.getPassword())

            System.setProperty("spring.flyway.url", POSTGRESQL_CONTAINER.getJdbcUrl())
            System.setProperty("spring.flyway.user", POSTGRESQL_CONTAINER.getUsername())
            System.setProperty("spring.flyway.password", POSTGRESQL_CONTAINER.getPassword())
        }
    }

    override fun beforeTestMethod(testContext: TestContext) {
        val flyway = testContext.applicationContext.getBean(Flyway::class.java)
        flyway.clean()
        flyway.migrate()
    }

}