CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE products
(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name varchar(255),
    price double precision
);