package fr.btravers.eshop.config.flyway

import org.flywaydb.core.Flyway
import org.springframework.boot.autoconfigure.flyway.FlywayProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(FlywayProperties::class)
class FlywayConfiguration {

    @Bean(initMethod = "migrate")
    fun flyway(flywayProperties: FlywayProperties) =
            Flyway(
                    Flyway.configure()
                            .baselineOnMigrate(true)
                            .dataSource(flywayProperties.url, flywayProperties.user, flywayProperties.password)
            )

}