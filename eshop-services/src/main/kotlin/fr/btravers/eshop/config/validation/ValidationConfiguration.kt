package fr.btravers.eshop.config.validation

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.StandardReflectionParameterNameDiscoverer
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean

@Configuration
class ValidationConfiguration {

    /**
     * FIXME https://github.com/spring-projects/spring-framework/issues/23499
     */
    @Bean
    @Primary
    fun validator(): LocalValidatorFactoryBean {
        val factoryBean = LocalValidatorFactoryBean()
        factoryBean.setParameterNameDiscoverer(StandardReflectionParameterNameDiscoverer())
        return factoryBean
    }

}