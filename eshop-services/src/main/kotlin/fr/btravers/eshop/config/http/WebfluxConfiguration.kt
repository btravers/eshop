package fr.btravers.eshop.config.http

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.lang.NonNull
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
class WebfluxConfiguration {

    @Bean
    fun webFluxConfigurer(mapper: ObjectMapper) =
            object : WebFluxConfigurer {

                override fun configureHttpMessageCodecs(@NonNull configurer: ServerCodecConfigurer) {
                    configurer.defaultCodecs().jackson2JsonDecoder(Jackson2JsonDecoder(mapper))
                    configurer.defaultCodecs().jackson2JsonEncoder(Jackson2JsonEncoder(mapper))
                }

            }

}