package fr.btravers.eshop.product.application

import fr.btravers.eshop.http.dto.ProductDto
import fr.btravers.eshop.product.Product
import org.springframework.stereotype.Component

@Component
class ProductDtoMapper {

    fun toProductDto(product: Product) =
            ProductDto(
                    id = product.id,
                    name = product.name,
                    price = product.price.intValueExact()
            )

    fun toProduct(productDto: ProductDto) =
            Product(
                    id = productDto.id,
                    name = productDto.name!!,
                    price = productDto.price!!.toBigInteger()
            )

}