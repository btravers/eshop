package fr.btravers.eshop.product.infrastructure

import fr.btravers.eshop.product.Product
import org.springframework.stereotype.Component

@Component
class ProductEntityMapper {

    fun toProductEntity(product: Product) =
            ProductEntity(
                    id = product.id,
                    name = product.name,
                    price = product.price
            )

    fun toProduct(productEntity: ProductEntity) =
            Product(
                    id = productEntity.id,
                    name = productEntity.name,
                    price = productEntity.price
            )

}