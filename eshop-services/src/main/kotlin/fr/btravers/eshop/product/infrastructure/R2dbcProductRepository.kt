package fr.btravers.eshop.product.infrastructure

import fr.btravers.eshop.product.Product
import fr.btravers.eshop.product.ProductRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactive.awaitSingleOrNull
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
class R2dbcProductRepository(private val productRepository: SpringDataR2dbcProductRepository,
                             private val productEntityMapper: ProductEntityMapper) : ProductRepository {

    override suspend fun save(product: Product): Product {
        return Mono.just(product)
                .map(productEntityMapper::toProductEntity)
                .flatMap(productRepository::save)
                .map(productEntityMapper::toProduct)
                .awaitSingle()
    }

    override fun findAll(): Flow<Product> {
        return productRepository.findAll()
                .map(productEntityMapper::toProduct)
                .asFlow()
    }

    override suspend fun findById(id: UUID): Product? {
        return productRepository.findById(id)
                .map(productEntityMapper::toProduct)
                .awaitSingleOrNull()
    }

    override suspend fun existsById(id: UUID): Boolean {
        return productRepository.existsById(id)
                .awaitSingle()
    }

    override suspend fun deleteById(id: UUID) {
        return productRepository.deleteById(id)
                .thenReturn(Unit)
                .awaitSingle()
    }

}