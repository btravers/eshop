package fr.btravers.eshop.product.infrastructure

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.math.BigInteger
import java.util.*

@Table("products")
data class ProductEntity(
        @Id val id: UUID? = null,
        val name: String,
        val price: BigInteger
)
