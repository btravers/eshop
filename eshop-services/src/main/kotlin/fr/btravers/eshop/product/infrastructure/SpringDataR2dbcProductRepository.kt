package fr.btravers.eshop.product.infrastructure

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import java.util.*

interface SpringDataR2dbcProductRepository : ReactiveCrudRepository<ProductEntity, UUID>