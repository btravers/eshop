package fr.btravers.eshop.product.application

import fr.btravers.eshop.http.ProductsApi
import fr.btravers.eshop.http.dto.ProductDto
import fr.btravers.eshop.product.ProductService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.util.*

@RestController
class ProductController(private val productService: ProductService,
                        private val productDtoMapper: ProductDtoMapper) : ProductsApi {

    override suspend fun addProduct(productDto: ProductDto): ResponseEntity<Unit> {
        return productDtoMapper.toProduct(productDto)
                .let { product -> productService.addProduct(product) }
                .let { savedProduct -> ResponseEntity.created(URI.create("/products/${savedProduct.id}")).build() }
    }

    override fun getAllProducts(): ResponseEntity<Flow<ProductDto>> {
        return productService.getAllProducts()
                .map { product -> productDtoMapper.toProductDto(product) }
                .let { result -> ResponseEntity.ok(result) }
    }

    override suspend fun getProductById(productId: UUID): ResponseEntity<ProductDto> {
        return productService.getProductById(productId)
                .let { product -> productDtoMapper.toProductDto(product) }
                .let { result -> ResponseEntity.ok(result) }
    }

    override suspend fun deleteProductById(productId: UUID): ResponseEntity<Unit> {
        productService.deleteProductById(productId)
        return ResponseEntity.noContent().build()
    }

    override suspend fun updateProductById(productId: UUID, productDto: ProductDto): ResponseEntity<ProductDto> {
        return productDtoMapper.toProduct(productDto)
                .let { product -> productService.updateProductById(productId, product) }
                .let { savedProduct -> productDtoMapper.toProductDto(savedProduct) }
                .let { result -> ResponseEntity.ok(result) }
    }

}