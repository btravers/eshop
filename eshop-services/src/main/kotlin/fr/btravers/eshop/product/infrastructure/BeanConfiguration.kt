package fr.btravers.eshop.product.infrastructure

import fr.btravers.eshop.product.ProductRepository
import fr.btravers.eshop.product.ProductService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BeanConfiguration {

    @Bean
    fun productService(productRepository: ProductRepository): ProductService {
        return ProductService(productRepository)
    }

}